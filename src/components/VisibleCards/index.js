import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Card from '../Card';
import style from './style.css';

const matches = (cardFilter, c) => (c.front.includes(cardFilter) || c.back.includes(cardFilter));

const mapStateToProps = ({ cards, cardFilter }, { params: { deckId } }) => ({
  cards: cards.filter(c => c.deckId === deckId && matches(cardFilter, c))
});

const Cards = ({ cards, children }) => (
  <div className="main">
    <div className="main-card">
      {cards.map(card => <Card card={card} key={card.id} />)}
      {children}
    </div>
  </div>
    );

Cards.defaultProps = {
  children: {}
};

Cards.propTypes = {
  cards: PropTypes.array.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.bool.isRequired
  ])
};

export default connect(mapStateToProps)(Cards);
