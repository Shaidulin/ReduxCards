import  {cardFilter} from '../../src/reducers';

let state = {};
let mockAction = {type:undefined, data:'mockData'};

describe(`reducer cardFilter test`, () => {
    it(`cardFilter state is undefined,action is undefined`, () => {
        expect( cardFilter(undefined, {}) ).toEqual('')
    });

    it(`cardFilter's state has property data, action has properties data and type`, () => {
        state = {
            data:'data'
        };
        expect( cardFilter(state,mockAction)).toEqual(state);
    });

    it(`cardFilter's state is undefined, action  has properties data and type`, () => {
        expect( cardFilter(undefined,mockAction)).toEqual('');
    });

    it(`cardFilter action.data is data, action.type is FILTER_CARDS, action.type is string `, () => {
        const action = {
            data:'data',
            type:'FILTER_CARDS'
        };
        expect(cardFilter(state, action)).toEqual('data');
    });
});
