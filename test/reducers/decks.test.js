import  {decks} from '../../src/reducers';

describe('reducer decks test', () => {
    it(`decks state is undefined,action is true`, () => {
        const state = {data:true};
        const action = {type:undefined, data:'mockData'};
        expect( decks(state, action) ).toEqual(state)
    });

    it(`decks action.type is undefined, action.data is undefined`, () => {
        expect( decks(undefined, {}) ).toEqual([])
    });

    it(`decks state is empty array.Decks action.type is RECEIVE_DATA, action.data is decks:'data' `, () => {
        const state = [];

        const action = {
            data:{decks:'data'},
            type:'RECEIVE_DATA'
        };
        expect(decks(state, action)).toEqual('data');
    });

    it(`decks state is empty array.Decks action.type is RECEIVE_DATA, action.data.decks is undefined' `, () => {
        const state = [];

        const action = {
            data:{decks:undefined},
            type:'RECEIVE_DATA'
        };
        expect(decks(state, action)).toEqual([]);
    });

    it(`deck's state has properties decks,scope,id. Action has properties type as ADD_DECK,data as {decks:'decksSecond',scope:1}`, () => {
        const action = {
            data: {
                decks:'decksSecond',
                score: 1,
            },
            id: +new Date,
            type:'ADD_DECK',
        };

        const state = [{
            decks:'decks',
            score: 1,
            id: +new Date
        }];

        const finalState = [
            {
                decks:'decks',
                score: 1,
                id: +new Date
            },
            {
                name: {
                    decks:'decksSecond',
                    score: 1
                },
                id: +new Date
            },
        ];

        expect(decks(state, action)).toEqual(finalState);
    });
});