import  {addingDeck} from '../../src/reducers';

describe('reducer addingDeck test', () => {
    it(`addingDeck state is undefined,action is undefined`, () => {
        expect( addingDeck(undefined, {}) ).toEqual(false)
    });

    it(`addingDeck state is object,action.type is SHOW_ADD_DECK`, () => {
        const state = {};
        const action = {
            type: 'SHOW_ADD_DECK',
        };
        expect( addingDeck(state, action) ).toEqual(true)
    });

    it(`addingDeck state is object,action.type is HIDE_ADD_DECK`, () => {
        const state = {};
        const action = {
            type: 'HIDE_ADD_DECK',
        };
        expect( addingDeck(state, action) ).toEqual(false)
    });
});