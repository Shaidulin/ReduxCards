###  Simle redux cards
Приложение для  облегчения запоминания слов с помощью интервальных повторений. В качестве основы использует флешкарты с пользовательскими данными.
Реализованы возможности добавления карточек, их интервального повторения, редактирования заполненных полей, добавление новых колод.

 - Содержит Eslint и тесты
 - Использует React / Redux / Webpack
 - Применяет express.js как сервер

# Installation

```bash
git clone git@gitlab.com:Shaidulin/ReduxCards.git
npm install
```

# Development

```bash
npm start
localhost:8090
```

# Server

```bash
npm run server
localhost:3333
```

# Lint
```bash
npm run lint
```
# Tests
```bash
npm test
```

![](0VfyVUwt2G.gif)
