import  {showBack} from '../../src/reducers';

let state = {};
let mockAction = {type:undefined, data:'mockData'};

describe(`reducer showBack test`, () => {

    it(`showBack's state is data:true, action has properties data`, () => {
        state = {data:true};
        expect(showBack(state, mockAction)).toEqual(state);
    });

    it(`showBack action.type is undefined, action.data is undefined`, () => {
        expect( showBack(undefined, {}) ).toEqual(false);
    });

    it(`showBack state is empty object, action.type is SHOW_BACK, action.data is true`, () => {
        const action = {
            data:true,
            type:'SHOW_BACK'
        };
        expect(showBack(state, action)).toEqual(true);
    });

    it(`showBack state is empty object, showBack action.type is SHOW_BACK, action.data is undefined`, () => {
        const action = {
            type:'SHOW_BACK'
        };
        expect(showBack(state, action)).toEqual(false);
    });

});

