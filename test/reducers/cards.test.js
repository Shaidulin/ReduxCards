import  {cards} from '../../src/reducers';

describe(`reducer cards test`, () => {
    it(`card's state is undefined,action is undefined`, () => {
        expect( cards(undefined, {}) ).toEqual([])
    });

    it(`card's state has properties data,scope,id, action has properties type`, () => {
        const action = {type:undefined, data:'mockData'};
        const state = [{
            data:'data',
            score: 1,
            id: new Date
        }];
        expect( cards(state,action)).toEqual(state);
    });

    it(`card's state is undefined, action has properties data and type`, () => {
        const action = {type:undefined, data:'mockData'};
        expect( cards(undefined,action)).toEqual([]);
    });

    it(`card's state has empty array, action has properties type as RECEIVE_DATA,data`, () => {
        const state = [];

        const action = {
            data:'data',
            type:'RECEIVE_DATA'
        };
        expect(cards(state, action)).toEqual(state);
    });

    it(`card's action.data.card is cards, action has properties type as RECEIVE_DATA,data as cards:data`, () => {
        const state = [];

        const action = {
            data:{cards:'cards'},
            type:'RECEIVE_DATA'
        };
        expect(cards(state, action)).toEqual('cards');
    });

    it(`card's state has properties cards,scope,id. Action has properties type as ADD_CARD,data as cards:cards`, () => {
        const action = {
            data: {cards:'cards'},
            type:'ADD_CARD'
        };

        const state = [{
            cards:'cards',
            score: 1,
            id: +new Date
        }];

        const finalState = [
            {
                cards:'cards',
                score: 1,
                id: +new Date
            },
            {
                cards:'cards',
                score: 1,
                id: +new Date
            },
        ];

        expect(cards(state, action)).toEqual(finalState);
    });

    it(`card's state has properties cards,scope,id. Action has properties type as UPDATE_CARD,data as cards:cardsNew`, () => {
        const action = {
            data: {cards:'cardsNew',id: +new Date},
            score: 1,
            type:'UPDATE_CARD'
        };

        const state = [{
            cards:'cards',
            score: 1,
            id: +new Date
        }];

        const finalState = [
            {
                cards:'cardsNew',
                score: 1,
                id: +new Date
            }];


        expect(cards(state, action)).toEqual(finalState);
    });

    it(`card's state has properties cards,scope,id. Action has properties type as DELETE_CARD,data as 111`, () => {
        const action = {
            data:  111,
            type:'DELETE_CARD'
        };

        const state = [{
            cards: 'cards',
            score: 1,
            id: +new Date
        },
        {
            cards: 'cardsSecond',
            score: 1,
            id: 111
         }];


        const finalState = [
            {
                cards: 'cards',
                score: 1,
                id: +new Date
            }
        ];

        expect(cards(state, action)).toEqual(finalState);
    });
});