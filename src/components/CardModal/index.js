import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Save from 'material-ui/svg-icons/action/done';
import Cansel from 'material-ui/svg-icons/content/clear';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Delete from 'material-ui/svg-icons/action/delete-forever';
import IconButton from 'material-ui/IconButton';
import PropTypes from 'prop-types';
import style from './style.css';

class CardModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: true,
      front: '',
      back: '',
      frontDisplayError: false,
      backDisplayError: false
    };
  }

  onSave() {
    const validation = (...state) => state.every(state => {
      return state !== undefined &&
        state.length > 3 &&
        state.length < 9 &&
        !state.match(/^$|\s+/g);
    });

    const { front, back } = this.state;
    const [frontEdit, backEdit] = [this.props.card.front, this.props.card.back];

    const editValidation = (previousValue, nextValue) => nextValue !== '' ? nextValue : previousValue;

    const [frontResultCheck, backResultCheck] = [validation(editValidation(frontEdit, front)),
      validation(editValidation(backEdit, back))];

    this.setState({ frontDisplayError: !frontResultCheck });
    this.setState({ backDisplayError: !backResultCheck });

    if (frontResultCheck && backResultCheck) {
      this.props.onSave(Object.assign({}, this.props.card, {
        front: editValidation(frontEdit, front),
        back: editValidation(backEdit, back)
      }));
      browserHistory.push(`/deck/${this.props.card.deckId}`);
    }
  }
  onDelete() {
    this.props.onDelete(this.props.card.id);
    browserHistory.push(`/deck/${this.props.card.deckId}`);
  }

  handleClose() {
    this.setState({ open: false });
    browserHistory.push(`/deck/${this.props.card.deckId}`);
  }

  handleOpen() {
    this.setState({ open: true });
  }

  render() {
    const { card } = this.props;
    const styleCansel = {
      float: 'right'
    };

    const actions = (
      <div>
        { this.props.onDelete ?
          <FloatingActionButton
            onClick={this.onDelete.bind(this)}
            className="delete"
            backgroundColor="#F44336"
          >
            <Delete />
          </FloatingActionButton>
                    : null }

        <FloatingActionButton
          onClick={this.onSave.bind(this)}
          className="change-style"
          backgroundColor="#2ecc71"
        >
          <Save />
        </FloatingActionButton>
      </div>
        );
    const titleAppBar = (
      <div>
        <Link style={styleCansel} to={`/deck/${card.deckId}`}>
          <IconButton>
            <Cansel />
          </IconButton>
        </Link>
      </div>
        );

    const contentDialog = (
      <div>
        <TextField
          errorText={this.state.frontDisplayError ? 'Your title must contains 4-8 symbols (no spaces)' : false}
          fullWidth
          onChange={e => this.setState({ front: e.target.value })}
          defaultValue={card.front}
          floatingLabelText="Card Front"
        />
        <TextField
          errorText={this.state.backDisplayError ? 'Your title must contains 4-8 symbols (include spaces)' : false}
          fullWidth
          onChange={e => this.setState({ back: e.target.value })}
          defaultValue={card.back}
          floatingLabelText="Card Back"
        />
      </div>);
    return (
      <Dialog
        title={titleAppBar}
        modal={false}
        open={this.state.open}
        onRequestClose={this.handleClose.bind(this)}
        actions={actions}
      >
        {contentDialog}
      </Dialog>
    );
  }
}

CardModal.defaultProps = {
  onDelete: () => {}
};

CardModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
  onDelete: PropTypes.oneOfType([
    PropTypes.func.isRequired,
    PropTypes.bool.isRequired
  ])
};

export default CardModal;
