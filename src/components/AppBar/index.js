import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import AppBar from 'material-ui/AppBar';
import ContentAdd from 'material-ui/svg-icons/content/add';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import AutoComplete from 'material-ui/AutoComplete';
import PropTypes from 'prop-types';

import { showAddDeck, filterCards } from '../../actions';

const mapDispatchToProps = dispatch => ({
  showAddDeck: () => dispatch(showAddDeck()),
  onFilter: query => dispatch(filterCards(query))
});


const dataSource3 = [
    { textKey: 'Some Text', valueKey: 'someFirstValue' },
    { textKey: 'Some Text', valueKey: 'someSecondValue' }
];

const dataSourceConfig = {
  text: 'textKey',
  value: 'valueKey'
};

const styleBar = {
  backgroundColor: '#f9f9f9'
};

const styleButton = {
  color: 'rgba(0, 0, 0, 0.298039)'
};

const defaultStyle = {
  color: 'white',
  backgroundColor: 'white'
};

const MainBar = ({ deckId, showAddDeck, onFilter }) => {
  const addDeck = (
    <FloatingActionButton
      onClick={showAddDeck}
      mini
      style={styleButton}
      backgroundColor="#2ecc71"
    >
      <ContentAdd />
    </FloatingActionButton>
    );

  const autoComplete = (
    <AutoComplete
      onUpdateInput={e => onFilter(e)}
      menuStyle={defaultStyle}
      errorStyle={defaultStyle}
      hintText="Cards search"
      openOnFocus
      dataSource={dataSource3}
      dataSourceConfig={dataSourceConfig}
    />
    );

  const deckTools = deckId ? (
    <div>
      <Link to={`/deck/${deckId}/new`}>
        <FlatButton
          style={styleButton}
          label="New card"
        />
      </Link>
      <Link to={`/deck/${deckId}/study`}>
        <FlatButton
          style={styleButton}
          label="Study cards"
        />
      </Link>
      {autoComplete}
    </div>
        ) : null;
  return (
    <div>
      <AppBar
        title={<span style={styleButton}>Simple redux cards</span>}
        iconElementLeft={addDeck}
        style={styleBar}
        iconElementRight={deckTools}
      />
    </div>
  );
};

MainBar.defaultProps = {
  deckId: ''
};

MainBar.propTypes = {
  deckId: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.bool.isRequired
  ]),
  showAddDeck: PropTypes.func.isRequired,
  onFilter: PropTypes.func.isRequired
};


export default connect(null, mapDispatchToProps)(MainBar);
