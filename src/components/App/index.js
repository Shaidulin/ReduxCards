import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Sidebar from '../Sidebar';
import AppBar from '../AppBar';

const mapStateToProps = (props, { params: { deckId } }) => ({
  deckId
});

const App = ({ deckId, children }) => (
  <div className="app">
    <AppBar deckId={deckId} />
    <Sidebar />
    {children}
  </div>
    );

App.defaultProps = {
  deckId: '',
  children: {}
};

App.propTypes = {
  deckId: PropTypes.oneOfType([
    PropTypes.string.isRequired,
    PropTypes.bool.isRequired
  ]),
  children: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.bool.isRequired
  ])
};

export default connect(mapStateToProps)(App);
