import React from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import Cansel from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import Flip from 'material-ui/svg-icons/action/cached';
import Neitral from 'material-ui/svg-icons/social/sentiment-neutral';
import Good from 'material-ui/svg-icons/social/sentiment-satisfied';
import Great from 'material-ui/svg-icons/social/mood';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import PropTypes from 'prop-types';
import { style } from './style.css';
import { updateCard, setShowBack } from '../../actions';

const MS_IN_DAY = 86400000;

const mapStateToProps = ({ cards, showBack }, { params: { deckId } }) => ({
  showBack,
  deckId,
  card: cards.filter(card => card.deckId === deckId &&
    (!card.lastStudiedOn || (new Date() - card.lastStudiedOn) / MS_IN_DAY >= card.score))[0]
});

const mapDispatchToProps = () => dispatch => ({
  onStudied: (cardId, score) => {
    const now = new Date();
    now.setHours(0, 0, 0, 0);
    dispatch(updateCard({ id: cardId, score, lastStudiedOn: +now }));
    dispatch(setShowBack());
  },
  onFlip: () => dispatch(setShowBack(true))
});


class StudyModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: true,
      front: '',
      back: ''
    };
  }

  handleOpen() {
    this.setState({ open: true });
  }

  handleClose() {
    this.setState({ open: false });
    browserHistory.push(`/deck/${this.props.deckId}`);
  }

  render() {
    const styleCansel = {
      float: 'right'
    };

    const styleFlip = {
      marginRight: '8px'
    };

    const { card, showBack, onFlip, deckId, onStudied } = this.props;

    let body = (
      <div className="no-cards">
        <p>You have no cards to study in this deck right now. Good job! </p>
      </div>);

    const flipButton =
             (<FloatingActionButton
               onClick={onFlip}
               style={styleFlip}
               backgroundColor="#2ecc71"
             >
               <Flip />
             </FloatingActionButton>
         );
    if (card) {
      body = (
        <div className="study-card">
          <div className={showBack ? 'front hide' : 'front'}>
            <div>
              <p>{card.front}</p>
            </div>
          </div>
          <div className={showBack ? 'back' : 'back hide'}>
            <div>
              <p>{card.front}</p>
              <p>{card.back}</p>
              <Divider />
            </div>
            <div>
              <p> How did you do?</p>
              <RaisedButton
                label="Poorly"
                className="button-poorly"
                onTouchTap={() => onStudied(card.id, Math.max(card.score - 1, 1))}
                icon={<Neitral />}
              />
              <RaisedButton
                label="Ok"
                className="button-normal"
                onTouchTap={() => onStudied(card.id, card.score)}
                icon={<Good />}
              />
              <RaisedButton
                className="button-great"
                label="Great"
                onTouchTap={() => onStudied(card.id, Math.min(card.score + 1, 3))}
                icon={<Great />}
              />
            </div>
          </div>
        </div>
             );
    }
    const titleAppBar = (
      <div>
        <Link style={styleCansel} to={`/deck/${deckId}`}>
          <IconButton>
            <Cansel />
          </IconButton>
        </Link>
      </div>
    );

    const contextModal = (
      <div className="modal study-modal">
        {body}
      </div>
         );

    return (
      <div>
        <Dialog
          title={titleAppBar}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose.bind(this)}
          actions={card && !showBack ? flipButton : null}
        >
          {contextModal}
        </Dialog>

      </div>
    );
  }
}

StudyModal.defaultProps = {
  card: false,
  showBack: false
};

StudyModal.propTypes = {
  deckId: PropTypes.string.isRequired,
  card: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.bool.isRequired
  ]),
  showBack: PropTypes.oneOfType([
    PropTypes.func.isRequired,
    PropTypes.bool.isRequired
  ]),
  onFlip: PropTypes.func.isRequired,
  onStudied: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(StudyModal);
