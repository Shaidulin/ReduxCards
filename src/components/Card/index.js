import React from 'react';
import { Link } from 'react-router';
import { Card, CardText } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import Edit from 'material-ui/svg-icons/editor/mode-edit';
import IconButton from 'material-ui/IconButton';
import PropTypes from 'prop-types';
import style from './style.css';

const styleButton = {
  float: 'right'
};

const styleCard = {
  padding: '26px 26px',
  height: '50px',
  fontSize: '17px'
};

const styleCardBack = {
  padding: '30px 30px',
  height: '50px',
  fontSize: '17px'
};

const CardDefault = ({ card }) => {
  const editCard = (
    <IconButton
      style={styleButton}
    >
      <Link className="btn" to={`/deck/${card.deckId}/edit/${card.id}`}>
        <Edit />
      </Link>
    </IconButton>
     );
  return (
    <div className="card">
      <Card>
        {editCard}
        <CardText style={styleCard}>{card.front.toLowerCase()}</CardText>
        <Divider />
        <CardText style={styleCardBack} >{card.back.toLowerCase()} </CardText>
      </Card>
    </div>
  );
};

CardDefault.propTypes = {
  card: PropTypes.object.isRequired
};

export default CardDefault;
