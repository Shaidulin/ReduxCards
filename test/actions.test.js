/**
 * Created by Александр on 5/13/2017.
 */
import {
    showAddDeck,
    addDeck,
    hideAddDeck,
    addCard,
    updateCard,
    deleteCard,
    filterCards,
    setShowBack,
    receiveData
} from '../src/actions';

describe('testing actions',() => {

    test('action addDeck',()=>{
        const name = 'name';
        expect(addDeck(name)).toEqual({type:'ADD_DECK',data:name});
    });

    test('action showAddDeck',()=>{
        expect(showAddDeck()).toEqual({type:'SHOW_ADD_DECK'});
    });

    test('action hideAddDeck',()=>{
        expect(hideAddDeck()).toEqual({type:'HIDE_ADD_DECK'});
    });

    test('action updateCard',()=>{
        const card = 'card';
        expect(updateCard(card)).toEqual({type:'UPDATE_CARD',data:card});
    });

    test('action addCard',()=>{
        const card = 'card';
        expect(addCard(card)).toEqual({type:'ADD_CARD',data:card});
    });

    test('action deleteCard',()=>{
        const cardId = 'cardId';
        expect(deleteCard(cardId)).toEqual({type:'DELETE_CARD',data:cardId});
    });

    test('action filterCards',()=>{
        const query = 'query';
        expect(filterCards(query)).toEqual({type:'FILTER_CARDS',data:query});
    });

    test('action setShowBack',()=>{
        const back = 'back';
        expect(setShowBack(back)).toEqual({type:'SHOW_BACK',data:back});
    });

    test('action receiveData',()=>{
        const data = 'data';
        expect(receiveData(data)).toEqual({type:'RECEIVE_DATA',data:data});
    });

});