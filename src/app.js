import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { browserHistory, Router, Route } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import thunkMiddleware from 'redux-thunk';
import * as reducers from './reducers';
import { fetchData } from './actions';
import App from './components/App';
import EditCardModal from './components/EditCardModal';
import VisibleCards from './components/VisibleCards';
import NewCardModal from './components/NewCardModal';
import StudyModal from './components/StudyModal';


reducers.routing = routerReducer;


const store = createStore(

combineReducers(reducers),
    applyMiddleware(thunkMiddleware)
);

const history = syncHistoryWithStore(browserHistory, store);
injectTapEventPlugin();

function run() {
  ReactDOM.render(
    <Provider store={store}>
      <MuiThemeProvider>
        <Router history={history}>
          <Route path="/" component={App}>
            <Route path="/deck/:deckId" component={VisibleCards}>
              <Route path="/deck/:deckId/new" component={NewCardModal} />
              <Route path="/deck/:deckId/edit/:cardId" component={EditCardModal} />
              <Route path="/deck/:deckId/study" component={StudyModal} />
            </Route>
          </Route>
        </Router>
      </MuiThemeProvider>
    </Provider>,

        document.getElementById('root'), // eslint-disable-line
    );
}


function save() {
  const state = store.getState();

  fetch('/api/data', { // eslint-disable-line
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      decks: state.decks,
      cards: state.cards
    })
  });
}

function init() {
  run();
  store.subscribe(run);
  store.subscribe(save);
  store.dispatch(fetchData());
}

init();
