import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Deck from 'material-ui/svg-icons/action/description';
import TextField from 'material-ui/TextField';
import PropTypes from 'prop-types';
import { addDeck, showAddDeck, hideAddDeck } from '../../actions';
import style from './style.css';

const mapStateToProps = ({ decks, addingDeck }) => ({
  decks,
  addingDeck
});

const mapDispatchToProps = dispatch => ({
  addDeck: name => dispatch(addDeck(name)),
  showAddDeck: () => dispatch(showAddDeck()),
  hideAddDeck: () => dispatch(hideAddDeck())
});

class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.validation = (...state) => state.every(state => {
      return state !== undefined &&
              state.length > 3 &&
              state.length < 9 &&
              !state.match(/^$|\s+/g);
    });

    this.state = {
      error: false
    };
  }

  checkFields(e) {
    return e.charCode === 13 && (this.validation(e.target.value) ?
            this.createDeck(e) :
            this.setState({ error: true }));
  }

  createDeck(e) {
    this.state.error = false;
    this.props.addDeck(e.target.value);
    this.props.hideAddDeck();
  }

  render() {
    const styleList = {
      width: '240px',
      display: 'inline-block',
      verticalAlign: 'top'
    };

    const styleItem = {
      'text-decoration': 'none'
    };

    const props = this.props;

    return (

      <div className="sidebar">
        <List style={styleList}>
          { props.addingDeck &&
          <TextField
            errorText={this.state.error ? 'Your title must contains 4-8 symbols (no spaces)' : false}
            onKeyPress={e => this.checkFields(e)}
            hintText="Hint Text"
          /> }
          {Array.from(props.decks)
            .sort((a, b) => (Array.from(props.decks).indexOf(a) < Array.from(props.decks).indexOf(b)) ? 1 : -1)
              .map((deck) => (
                <Link className="deck-list_item" key={deck.id} to={`/deck/${deck.id}`}>
                  <ListItem insetChildren primaryText={deck.name} rightIcon={<Deck />} />
                  <Divider inset={false} />
                </Link>
                ))
            }</List>
      </div>);
  }
}

Sidebar.propTypes = {
  addDeck: PropTypes.func.isRequired,
  hideAddDeck: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
